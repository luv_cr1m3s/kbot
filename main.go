/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import "github.com/cr1m3s/kbot/cmd"

func main() {
	cmd.Execute()
}
